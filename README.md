# Programa NPB-IS,EP,CG
## O que faz? Para que serve?
NPB (Nas Parallel Benchmarks) é uma suíte de benchmarks projetada para
(mas não limitada a) computadores paralelos.
Nela há diversos kernels, do qual escolhemos IS (Integer Sort),
EP (Embarassingly Parallel), CG (Conjugate Gradient) que serão rodados
em série como um só benchmark.
## Por que é bom para medir desempenho?
O conjunto escolhido faz uso de variados recursos do computador, entre eles:
acesso à memória (mais pelo IS), paralelismo (EP), e unidades de aritmética
de inteiros e ponto flutuante (CG).
Assim, este pode ser considerado um benchmark interessante para máquinas
que realizam muitas tarefas de computação numérica, pois esta faz uso intenso
dos recursos aqui testados.
## O que baixar
O pré-requisito para compilar este benchmark é GCC com suporte a OpenMP,
algo que qualquer computador com Linux provavelmente já tem.
Por ser pequeno, o código-fonte do benchmark já encontra-se todo
neste repositório.
O código é uma versão totalmente em C do NPB (alguns programas da suíte original
são em Fortran), na versão 3.0. Ele pode ser encontrado
[neste](https://github.com/benchmark-subsetting/NPB3.0-omp-C) repositório.
Usamos o commit *5d565d916cad47bf09a2fa7432bf8e5fa95446ec*.
## Como compilar/instalar
Basta rodar o script **build.sh**, que encontra-se na raiz deste repositório.
## Como executar
Basta rodar o script **run.sh**, que encontra-se na raiz deste repositório.
## Como medir o desempenho
O desempenho é feito através do tempo de execução real (wall time).
Diversas medidas devem ser feitas e depois deve calcular-se a média.
Deve ser feito um número de medidas que provenha uma média estatisticamente
significante.
Um número razoável é 16 medições.
## Como apresentar o desempenho
Supondo uma distribuição do tempo similar a uma normal
(geralmente é mais próxima de uma long tail, mas é uma aproximação razoável),
pode-se calcular o intervalo de confiança (de 95%) da média adquirida.
Tal intervalo dá uma região de confiança de onde a "média real" deve estar.
Assim, o valor é apresentado como uma média e o intervalo de confiança em torno
dela.
## Medições base (uma máquina)
Como máquina base foi usado um notebook com Intel i3-4010U de 1.70GHz e 64 bits
de dois núcleos físicos
e hyperthreading, 4GB de RAM e sistema operacional
Arch Linux com Kernel Linux 4.10.1-1.
Os resultados são:
+ média: 6.1379s
+ intervalo de confiança: entre 6.0431s e 6.2327s

Obs: note que o tempo é menos de 10s mas o tempo de CPU foi em torno de 16s.

## Extra: Extraindo a métrica
Para extrair a métrica (média de tempo, intervalo de confiança), basta
executar o script **measure.sh**, que roda o benchmark várias vezes e tira
as estatísticas.
Precisa de Python 3, numpy e scipy.

