#!/usr/bin/env python3

import sys
import math

def mean(arr):
    return sum(arr)/max(len(arr), 1)

def var(arr):
    u = mean(arr)
    return mean(list(((x - u)**2 for x in arr)))

def std(arr):
    return math.sqrt(var(arr))

def sem(arr):
    n = max(len(arr), 1)
    sd = std(arr)
    return sd/math.sqrt(n)

def ci(arr):
    #z for 0.95 with n~16
    z = 2.150
    u = mean(arr)
    err = z*sem(arr)
    return err

def file_to_arr(filepath, header=True):
    with open(filepath) as f:
        lines = [l.strip() for l in f]
    lines = [l for l in lines if l]
    return ((lines[0] if header else ""),
        list(map(float, (lines[int(header):]))))

def stats():
    if len(sys.argv) < 2:
        print("usage: stats <csv_file>")
        exit()

    header = True
    col, data = file_to_arr(sys.argv[1], header)

    print("\tmin: {:6f}".format(min(data)))
    print("\tmax: {:6f}".format(max(data)))
    print("\tmean: {:6f}".format(mean(data)))
    print("\tstd: {:6f}".format(std(data)))
    print("\tconfidence interval (95%): {:6f}".format(ci(data)))
    print("\tCONF IVAL MIN, CONF IVAL MAX: {:6f}, {:6f}".format(
        mean(data) - ci(data), mean(data) + ci(data)))

def main():
    stats()

if __name__ == "__main__":
    main()
