#!/bin/bash

#build.sh -- benchmark builder
#this script compiles three programs used in benchmark-mc733, storing them
#in a binaries directory.

#directory of this script
file_dir="$(dirname "${BASH_SOURCE[0]}")"
#directory for binaries
bin_dir=$file_dir"/bin"
#directory for source code
src_dir=$file_dir"/src/NPB3.0-omp-C"

#making bin dir
mkdir -p -- $bin_dir

#building benchmarks
cd $src_dir
echo "building is..."
make is CLASS=A || { echo "error building is"; exit 1; }
echo "building ep..."
make ep CLASS=W || { echo "error building ep"; exit 1; }
echo "building cg..."
make cg CLASS=A || { echo "error building cg"; exit 1; }

echo
echo "--------------------------------------"
echo "[build.sh] done"
