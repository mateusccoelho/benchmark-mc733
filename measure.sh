#!/bin/bash

n_its=16
run_out=$(mktemp)

export TIMEFORMAT="bm_time_real,%R"
for i in $(seq $n_its); do
    time ./run.sh
done 2>&1 | tee -a $run_out

times_out=$(mktemp)
echo time > $times_out
cat $run_out | grep "bm_time_real" | cut -f2 -d, >> $times_out

echo
echo "-------------"
echo "METRIC STATS:"
./bm_stats.py $times_out
