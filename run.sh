#!/bin/bash

#this is the benchmark executable.

#directory of this script
file_dir="$(dirname "${BASH_SOURCE[0]}")"
#directory for binaries
bin_dir=$file_dir"/bin"

if [[ ! -d $bin_dir ]]; then
    echo "$bin_dir does not exist, run build.sh first."
    exit 1
fi

for app in $bin_dir/*; do
    echo "in $app ..."
    $app
done
