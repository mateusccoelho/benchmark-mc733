/* CLASS = W */
/*
c  This file is generated automatically by the setparams utility.
c  It sets the number of processors and the class of the NPB
c  in this directory. Do not modify it by hand.
*/
#define	CLASS	 'W'
#define	M	25
#define	CONVERTDOUBLE	FALSE
#define COMPILETIME "16 Mar 2017"
#define NPBVERSION "3.0 structured"
#define CS1 "cc"
#define CS2 "cc"
#define CS3 "(none)"
#define CS4 "-I../common"
#define CS5 "-O3 -fopenmp -mtune=native -shared"
#define CS6 "-fopenmp -lm"
#define CS7 "randdp"
