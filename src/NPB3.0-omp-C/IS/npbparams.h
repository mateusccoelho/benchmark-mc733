#define CLASS 'A'
/*
   This file is generated automatically by the setparams utility.
   It sets the number of processors and the class of the NPB
   in this directory. Do not modify it by hand.   */
   
#define COMPILETIME "16 Mar 2017"
#define NPBVERSION "3.0 structured"
#define CC "cc"
#define CFLAGS "-O3 -fopenmp -mtune=native -shared"
#define CLINK "cc"
#define CLINKFLAGS "-fopenmp -lm"
#define C_LIB "(none)"
#define C_INC "-I../common"
